# this is where you register views first, then you go to the main
# tracker url file and do the same

from django.urls import path
from . import views
from .views import project_list


urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", views.project_detail, name="show_project"),
    path("create/", views.create_project, name="create_project"),
]
