from django.shortcuts import get_object_or_404, render, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm


# remember, this is a function based view, you did this instead of
# a class based view for purposes of ease and continuity


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    return render(
        request, "projects/project_list.html", {"projects": projects}
    )


@login_required
def project_detail(request, id):
    project = get_object_or_404(Project, pk=id)
    return render(
        request, "projects/project_detail.html", {"project": project}
    )


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")  # Correct name?
    else:
        form = ProjectForm()

    return render(request, "projects/create_project.html", {"form": form})


# if this doesn't work, try the inputs you have in that backup file
