from django import forms
from .models import Project

# Not sure if i needed a new form.py for this or not
# if this doesn't work try putting it in the other file


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
