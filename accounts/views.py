from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from .forms import LoginForm, SignUpForm
from django.contrib.auth.models import User


# this is viewing your login form.
# remember the import view you used in tracker/urls.py
# Is that related to this view? look that up if this doesnt work
# Urls.py may be the related one, check that too. unless you did this right.
# which is doubtful.


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data["username"],
                password=form.cleaned_data["password"],
            )
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = LoginForm()
    return render(request, "accounts/login.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            if (
                form.cleaned_data["password"]
                != form.cleaned_data["password_confirmation"]
            ):
                form.add_error(
                    "password_confirmation", "The passwords do not match"
                )
            else:
                user = User.objects.create_user(
                    username=form.cleaned_data["username"],
                    password=form.cleaned_data["password"],
                )
                login(request, user)
                return redirect(reverse("list_projects"))
        return render(
            request, "accounts/registration/signup.html", {"form": form}
        )
    else:
        form = SignUpForm()
        return render(
            request, "accounts/registration/signup.html", {"form": form}
        )


# in that last line, "form" part is in place of "context"
# so form is the context I'm passing from my template.
# so that is why you dont have a context request in here (yet)
