from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

# this little piggy sets up the login info


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(widget=forms.PasswordInput)


# and this little piggy lets users signup


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )

    # try out the meta class here but tweak it if it doesn't work
    # didnt have it like this in 2shot but this is a more succinct way to
    # see what I'm creating to source in views.py and html

    # CORRECTION no that did not work but I still don't really understand
    # why it didn't work so make sure to look it up later why meta class
    # isn't always the best way to go
