from django.urls import path
from .views import login_view, logout_view
from .views import user_signup


# this helps to break down somewhere:
# path("nameofpage", html page template, name=sameas page)


urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", user_signup, name="signup"),
]
